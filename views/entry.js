import Vue from 'vue'
import App from './main.vue'
import VueRouter from 'vue-router'

import firstPage from './first-page.vue'
import threadPage from './thread-page.vue'
import singleThread from './single-thread.vue'

Vue.use(VueRouter)

const routes = [
    { path: '/thread/:id', component: singleThread },
    { path: '/board/', component: threadPage },
    { path: '/', component: firstPage }
]

const router = new VueRouter({
    routes
})

new Vue({
    router,
    render: h => h(App)
}).$mount('#main')
