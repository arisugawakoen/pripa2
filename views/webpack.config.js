const webpack = require("webpack")
const path = require("path")
const { VueLoaderPlugin } = require('vue-loader')

module.exports = {
    //mode: "development",
    mode: "production",
    entry: "./entry.js",
    output: {
        path: path.join(__dirname, '../public/javascripts'),
        filename: "bundle.js"
    },
    module: {
        rules: [
            { test: /\.vue$/, loader: "vue-loader" },
            { test: /\.css$/,
              use: [
                  'vue-style-loader',
                  'css-loader'
              ]
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin()
    ],
};
