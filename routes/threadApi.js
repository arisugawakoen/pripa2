'use strict'

const express = require('express')
const router = express.Router()
const models = require('../models')
const moment = require('moment')

// escape JavaScript and HTML

function escapeJsHTML(str) {
    return str
        .replace(/\\/g, '\\\\')
        .replace(/&/g, '&amp;')
        .replace(/'/g, '&#39;')
        .replace(/"/g, '&quot;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/(0x0D)/g, '\r')
        .replace(/(0x0A)/g, '\n')
}

// get all threads name

router.get('/all', (req, res) => {
    models.thread.sequelize.query(
        'select id, title, latest_res_number, create_date, update_date from threads order by update_date DESC;',
        { type: models.sequelize.QueryTypes.SELECT }
    ).then((response) => {
        return JSON.stringify(response)
    }).then((threads) => {
        res.json(threads)
    }).catch((e) => {
        res.status(400).json(e)
    })
})

// get latest threads name and post

router.get('/:offset(\\d+)/:limit(\\d+)', (req, res) => {
    const offset = parseInt(req.params.offset)
    const limit = parseInt(req.params.limit)
    const articleLimit = 10

    models.thread.sequelize.query(
        'select id, title, create_date, update_date, name, post from threads order by update_date DESC limit $2 offset $1;',
        { bind: [offset, limit], type: models.sequelize.QueryTypes.SELECT }
    ).then((threads) => {
        let threadArr = []
        let threadFunc = []
        threads.forEach((thread) => {
            threadFunc.push(new Promise((threadResolve, threadReject) => {
                models.article.sequelize.query(
                    'select * from (select * from articles where thread_id=$1 order by id DESC limit $2) as n order by id ASC;',
                    { bind: [thread.id, articleLimit],
                    type: models.sequelize.QueryTypes.SELECT }
                ).then((response) => {
                    return JSON.stringify(response)
                }).then((articles) => {
                    thread.articles = articles
                    threadArr.push(thread)
                }).then(() => {
                    threadResolve()
                }).catch((e) => {
                    console.log(e)
                    threadReject()
                })
            }))
        })
        Promise.all(threadFunc).then(() => {
            res.json(threadArr)
        })
    }).catch((e) => {
        console.log(e)
        res.status(400).json(e)
    })
})

// get a thread name and post

router.get('/id/:threadId(\\d+)', (req, res) => {
    const threadId = parseInt(req.params.threadId)

    models.thread.sequelize.query(
        'select id, title, create_date, update_date, name, post from threads where id = $1',
        { bind: [threadId], type: models.sequelize.QueryTypes.SELECT }
    ).then((thread) => {
        return JSON.stringify(thread[0])
    }).then((response) => {
        res.json(response)
    }).catch((e) => {
        res.status(400).json(e)
    })
})

// create a new thread

router.post('/', (req, res) => {
    const title = req.body.title ? escapeJsHTML(req.body.title) : ''
    const post = req.body.post ? escapeJsHTML(req.body.post) : ''
    const name = req.body.name ? escapeJsHTML(req.body.name) : ''

    if (req.body.title && req.body.post) {
        models.thread.sequelize.query(
            'insert into threads (title, create_date, update_date, latest_res_number, post, name, "createdAt", "updatedAt") values ($1, $2, $2, 1, $3, $4, $2, $2)',
            { bind: [title, moment().format('YYYY-MM-DD HH:mm:ss'), post, name]}
        ).then((response) => {
            res.status(204).json(response)
        }).catch((e) => {
            console.log(e)
            res.status(409).json(e)
        })
    } else {
        res.sendStatus(400)
    }
})

module.exports = router
